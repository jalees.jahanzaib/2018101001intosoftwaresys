def leap_year(k):
	if(k%100==0):
		if(k%400==0):
			return "true"
		else:
			return "false"
	else:
		if(k%4==0):
			return "true"
		else:
			return "false"
x=input()
print(bool(leap_year(x)))