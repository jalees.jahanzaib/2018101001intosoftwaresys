from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask import request
from flask import jsonify

app = Flask(_name_)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///test.db'
db = SQLAlchemy(app)

class User(db.Model):
    #id = db.Column(db.Integer, primary_key=True)
    rollnumber = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(120), unique=True)
    email = db.Column(db.String(120), unique=True)

    def _init_(self,rollnumber, name, email):
        self.rollnumber=rollnumber
        self.name = name
        self.email = email

    def _repr_(self):
        return '<User %r>' % self.name

@app.route("/students/create", methods=["GET","POST"])
def userAdd():
    rollnumber=request.form['rollnumber']
    name=request.form['name']
    email= request.form['email']
    db.create_all()
    new_person=User(rollnumber,name, email)
    db.session.add(new_person)
    db.session.commit()
    temp ={}
    temp['status']=(type(new_person)==User)
    return jsonify(temp)

@app.route("/students/",methods=["GET"])
def userFetch():
    db.create_all()
    allUsers=User.query.all()
    strf = '['
    for user in allUsers:
        strf += "{"+str(user.rollnumber)+" ," + str(user.name) + " ," + str(user.email) + " ," + "}" + " ,"
    strf += "]" 
    return strf

@app.route("/students/<rollnumber>", methods=["GET","POST"])
def userUpdate(rolln):
    db.create_all()
    allUsers=User.query.all()
    roll=request.form['rollnumber']
    name=request.form['name']
    email=request.form['email']
    new_person = User(roll, name, email)
    strf = ''
    for user in User.query.all():
        if user.rollnumber == rolln:
            db.session.delete(user)
            db.session.add(new_person)
            strf += '{student = {' + 'rollnumber:' + str(user.rollnumber) + ", " + 'name:' + str(user.name) + ", " + 'email:' + str(user.email) + '}' + "}\n"
    strf += '\n'
    db.session.commit()
    return strf

@app.route("/students/delete", methods=["POST"])
def userDelete():
    db.create_all()
    allUsers=User.query.all()
    roll=request.form['rollnumber']
    for user in User.query.all():
        if user.rollnumber == roll:
            db.session.delete(user)
    db.session.commit()
    return "Deleted rollnumber:" + str(roll) + "\n"


if _name_ == "_main_":
    app.run(host='127.0.0.1', port=5000)